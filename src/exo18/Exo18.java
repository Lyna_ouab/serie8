package exo18;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class Exo18 {
	public static void main(String[] args) {
		
		//pour �crire des object directement dans dans un .bin on utilise ObjectOutputStream
		//implementation de l'interface Serializable du java.io
		
		List<Person> perso= List.of(
				new Person ("Jacquet","Aim�",79),
				new Person ("Deschamps","Didier",51),
				new Person ("Thuram","Lilian",48),
				new Person ("Barthez","Fabien",49),
				new Person ("Zineddine","Ziden",47)
		);
        String fileName = "files/file2.bin";

        System.out.println("Check the file 'file2.bin' \n");
        try (PersonOutputStream personOutputStream = new PersonOutputStream(new FileOutputStream(fileName))) {
            personOutputStream.writePeople(perso);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try (PersonInputStream personInputStream = new PersonInputStream(new FileInputStream(fileName))) {
            List<exo18.Person> people = personInputStream.readPeople();
            people.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
