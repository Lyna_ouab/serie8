package exo16;

import java.util.List;

public class Exo16 {

	public static void main(String[] args) {
		
		PersonReader personReader = new PersonReader();
		List <Person> persons = personReader.read("files/Persons.txt");
		System.out.println(persons);
		
		PersonWriter personWriter = new PersonWriter();
		personWriter.write(persons, "files/Persons.txt");
		
	}

}

