package exo16;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.function.Function;

public class PersonWriter {
	Function <Person, String> toStream = 
			person -> {
				StringBuilder line = new StringBuilder();
				line.append(person.getFirstName() + ", " + person.getLastName() + ", " 
						+ person.getAge()+"\n");
				return line.toString(); 
			};
	
	
	public void write (List <Person> people, String filename) {
		File file=new File(filename);
		try(Writer writer =new FileWriter(file,true);
				BufferedWriter bufferedwriter=new BufferedWriter(writer)){
			bufferedwriter.write("\r");
			for(Person person:people) {
				bufferedwriter.write(toStream.apply(person));
			}
			
		}catch(IOException e) {
			e.printStackTrace();
		}
}
}