package exo16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PersonReader {

	Function<String, Person> toPerson =
            line -> {
     
					String[] element = line.split(",");
					return new Person (element[0],element[1],Integer.parseInt(element[2].trim()));
            };  
            
	public List<Person> read(String fileName){
		File file = new File(fileName);
		List <Person> persons = new  ArrayList<Person>();
		try (Reader reader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(reader)) {
			List <String> lines = bufferedReader.lines()
												 .filter(s -> !(s.startsWith("#") || s.isEmpty()))
												 .collect(Collectors.toList());
			for (String line : lines) {
					Person person = toPerson.apply(line);
					persons.add(person);
			} 
		} catch (IOException e) {
			e.printStackTrace();
		}
		return persons;
	}
}
