package exo17;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
public class Exo17 {

	public static void main(String[] args) {
		
		List<Person> perso= List.of(
					new Person ("Jacquet","Aim�",79),
					new Person ("Deschamps","Didier",51),
					new Person ("Thuram","Lilian",48),
					new Person ("Barthez","Fabien",49),
					new Person ("Zineddine","Ziden",47)
		);
		try(PersonOutputStream personOutputStream=new PersonOutputStream(new FileOutputStream("files/file1.bin"))){
			personOutputStream.writeFields(perso);
		}catch (IOException e) {
			e.printStackTrace();
		}
		
        try (PersonInputStream personInput = new PersonInputStream(new FileInputStream("files/file1.bin"))) {
            List<exo16.Person> perso1;
            perso1 = personInput.readFields();
            perso1.forEach(System.out::println);
        } catch (IOException e) {
			e.printStackTrace();
		}
		

	}

}
